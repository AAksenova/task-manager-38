package ru.t1.aksenova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseConnection();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

}

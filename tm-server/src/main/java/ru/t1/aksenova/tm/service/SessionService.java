package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.repository.IUserOwnedRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.ISessionService;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connection) {
        super(connection);
    }

    @Override
    public @NotNull IUserOwnedRepository<Session> getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

}

package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.api.repository.IRepository;
import ru.t1.aksenova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}

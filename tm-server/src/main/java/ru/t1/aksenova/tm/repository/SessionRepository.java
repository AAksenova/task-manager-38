package ru.t1.aksenova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    private static final String tableName = "tm_session";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    @SneakyThrows
    protected Session fetch(@NotNull ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setDate(row.getTimestamp("date"));
        session.setRole(Role.valueOf(row.getString("role")));
        return session;
    }

    @Override
    @SneakyThrows
    public Session add(@NotNull Session session) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, user_id, date, role) VALUES (?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setTimestamp(3, new Timestamp(session.getDate().getTime()));
            statement.setString(4, session.getRole().name());
            statement.executeUpdate();
        }
        return session;
    }

    @Override
    @SneakyThrows
    public @Nullable Session add(@Nullable String userId, @Nullable Session session) {
        session.setUserId(userId);
        return add(session);
    }

}

package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IProjectService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.SessionTestData.USER_SESSION_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService service = new ProjectService(connectionService);

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connectionService.getConnection());

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final User user = userRepository.add(USER_TEST);
        userId = user.getId();
        @NotNull final User admin = userRepository.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        ADMIN_PROJECT1.setUserId(adminId);
        ADMIN_PROJECT2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = userRepository.findOneById(userId);
        if (user != null) userRepository.removeOne(user);
        user = userRepository.findOneById(adminId);
        if (user != null) userRepository.removeOne(user);
    }

    @Before
    public void before() {
        service.add(USER_PROJECT1);
        service.add(USER_PROJECT2);
    }

    @After
    public void after() {
        service.removeAll(userId);
        service.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> service.add(NULL_PROJECT));
        Assert.assertNotNull(service.add(ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(ADMIN_PROJECT1.getStatus(), project.getStatus());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST) {
            @Nullable final Project project2 = service.findOneById(project.getId());
            Assert.assertEquals(project2.getName(), project.getName());
            Assert.assertEquals(project2.getId(), project.getId());
            Assert.assertEquals(project2.getUserId(), project.getUserId());
            Assert.assertEquals(project2.getStatus(), project.getStatus());
        }
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, ADMIN_PROJECT1));
        Assert.assertThrows(AbstractException.class, () -> service.add("", ADMIN_PROJECT1));
        Assert.assertThrows(AbstractException.class, () -> service.add(adminId, NULL_PROJECT));
        Assert.assertNotNull(service.add(adminId, ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(adminId, ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(ADMIN_PROJECT1.getStatus(), project.getStatus());
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(adminId, null, ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(adminId, ADMIN_PROJECT1.getName(), null));
        @NotNull final Project project = service.create(adminId, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(adminId, project.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(AbstractException.class, () -> service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        @NotNull final Project project = service.updateById(userId, USER_PROJECT1.getId(), PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    private int getIndexFromList(@NotNull final List<Project> projects, @NotNull final String id) {
        int index = 0;
        for (Project project : projects) {
            index++;
            if (id.equals(project.getId())) return index;
        }
        return -1;
    }

    @Test
    public void updateByUserIdByIndex() {
        @NotNull final List<Project> projects = service.findAll();
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(null, index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, -1, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, index, null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, index, USER_PROJECT1.getName(), null));
        @NotNull final Project project = service.updateByIndex(userId, index, PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(null, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(userId, USER_PROJECT1.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(userId, NON_EXISTING_PROJECT_ID, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusById(NON_EXISTING_USER_ID, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        @NotNull final Project project = service.changeProjectStatusById(userId, USER_PROJECT1.getId(), Status.IN_PROGRESS);
        @NotNull final Project project2 = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertEquals(project2.getId(), project.getId());
        Assert.assertEquals(project2.getStatus(), project.getStatus());
        Assert.assertEquals(project2.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final List<Project> projects = service.findAll();
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(userId, null, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(userId, index, null));
        Assert.assertThrows(AbstractException.class, () -> service.changeProjectStatusByIndex(userId, -1, Status.COMPLETED));
        @NotNull final Project project = service.changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void set() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final IProjectService emptyService = new ProjectService(connectionService);
        Assert.assertEquals(EMPTY_PROJECT_LIST, Collections.emptyList());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.set(ADMIN_PROJECT_LIST);
        final List<Project> projects2 = service.findAll();
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    @Test
    public void findAll() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final IProjectService emptyService = new ProjectService(connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        final List<Project> projects = emptyService.findAll();
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findAll(""));
        Assert.assertEquals(Collections.emptyList(), service.findAll(NON_EXISTING_USER_ID));
        final List<Project> projects = service.findAll(userId);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparator() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final IProjectService emptyService = new ProjectService(connectionService);
        emptyService.add(USER_PROJECT_LIST);
        emptyService.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Project> projects = service.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        final List<Project> projects2 = service.findAll(adminId, comparator);
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Project> projects = service.findAll();
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(-1));
        @Nullable final Project project = service.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Project> projects = service.findAll(userId);
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(USER_PROJECT1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(null, index));
        @Nullable final Project project = service.findOneByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(userId, null));
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null, USER_PROJECT1.getId()));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void removeAll() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final IProjectService emptyService = new ProjectService(connectionService);
        emptyService.add(PROJECT_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeOne() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOne(null));
        @Nullable final Project project = service.add(ADMIN_PROJECT1);
        Assert.assertNotNull(service.findOneById(ADMIN_PROJECT1.getId()));
        service.removeOne(project);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertNull(service.removeOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.add(ADMIN_PROJECT2);
        Assert.assertNotNull(service.findOneById(ADMIN_PROJECT2.getId()));
        service.removeOneById(project.getId());
        Assert.assertNull(service.findOneById(ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null, ADMIN_PROJECT2.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(adminId, null));
        Assert.assertNull(service.removeOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.add(ADMIN_PROJECT2);
        Assert.assertNotNull(service.findOneById(adminId, ADMIN_PROJECT2.getId()));
        service.removeOneById(project.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(adminId, ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        service.add(ADMIN_PROJECT1);
        @NotNull final List<Project> projects = service.findAll();
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        @Nullable final Project project2 = service.removeOneByIndex(index);
        Assert.assertNotNull(project2);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        @Nullable final Project project = service.add(ADMIN_PROJECT1);
        @NotNull final List<Project> projects = service.findAll(adminId);
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(USER_PROJECT1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(null, index));
        @Nullable final Project project2 = service.removeOneByIndex(adminId, index);
        Assert.assertNotNull(project2);
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(project2.getId(), project2.getUserId()));
    }

}

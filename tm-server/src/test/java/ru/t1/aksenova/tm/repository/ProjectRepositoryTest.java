package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IProjectRepository repository = new ProjectRepository(connection);

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final User user = userRepository.add(USER_TEST);
        userId = user.getId();
        @NotNull final User admin = userRepository.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        ADMIN_PROJECT1.setUserId(adminId);
        ADMIN_PROJECT2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = userRepository.findOneById(userId);
        if (user != null) userRepository.removeOne(user);
        user = userRepository.findOneById(adminId);
        if (user != null) userRepository.removeOne(user);
    }

    @Before
    public void before() {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @After
    public void after() {
        repository.removeAll(userId);
        repository.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project.getId(), repository.findOneById(project.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertNotNull(repository.add(adminId, ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(adminId, ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());

    }

    @Test
    public void createByUserId() {
        @NotNull final Project project = repository.create(adminId, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(adminId, project.getUserId());
    }

    @Test
    public void set() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.set(ADMIN_PROJECT_LIST);
        final List<Project> projects2 = repository.findAll();
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    private int getIndexFromList(@NotNull final List<Project> projects, @NotNull final String id) {
        int index = 0;
        for (Project project : projects) {
            index++;
            if (id.equals(project.getId())) return index;
        }
        return -1;
    }

    @Test
    public void findAll() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        final List<Project> projects = emptyRepository.findAll();
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Project> projects = repository.findAll(userId);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparator() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Project> projects = emptyRepository.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        final List<Project> projects2 = emptyRepository.findAll(adminId, comparator);
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Project> projects = repository.findAll();
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Project> projects = repository.findAll(userId);
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        @Nullable final Project project = repository.findOneByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void removeAll() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        emptyRepository.add(PROJECT_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Project project = repository.add(ADMIN_PROJECT1);
        Assert.assertNotNull(repository.findOneById(ADMIN_PROJECT1.getId()));
        repository.removeOne(project);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.add(ADMIN_PROJECT2);
        Assert.assertNotNull(repository.findOneById(ADMIN_PROJECT2.getId()));
        repository.removeOneById(project.getId());
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(adminId, NON_EXISTING_PROJECT_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_PROJECT2.getId()));
        @Nullable final Project project = repository.add(ADMIN_PROJECT2);
        Assert.assertNotNull(repository.findOneById(adminId, ADMIN_PROJECT2.getId()));
        repository.removeOneById(project.getId());
        Assert.assertNull(repository.findOneById(adminId, ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        repository.add(ADMIN_PROJECT1);
        @NotNull final List<Project> projects = repository.findAll();
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertNotNull(project);
        @Nullable final Project project2 = repository.removeOneByIndex(index);
        Assert.assertNotNull(project2);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        repository.add(ADMIN_PROJECT1);
        @NotNull final List<Project> projects = repository.findAll(adminId);
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        @Nullable final Project project2 = repository.removeOneByIndex(adminId, index);
        Assert.assertNotNull(project2);
        Assert.assertNull(repository.findOneById(project2.getId(), project2.getUserId()));
    }

}

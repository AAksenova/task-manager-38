package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository repository = new TaskRepository(connection);

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final User user = userRepository.add(USER_TEST);
        userId = user.getId();
        @NotNull final User admin = userRepository.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_TASK1.setUserId(userId);
        USER_TASK2.setUserId(userId);
        ADMIN_TASK1.setUserId(adminId);
        ADMIN_TASK2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = userRepository.findOneById(userId);
        if (user != null) userRepository.removeOne(user);
        user = userRepository.findOneById(adminId);
        if (user != null) userRepository.removeOne(user);
    }

    @Before
    public void before() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() {
        repository.removeAll(userId);
        repository.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task.getId(), repository.findOneById(task.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertNotNull(repository.add(adminId, ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(adminId, ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void createByUserId() {
        @NotNull final Task task = repository.create(adminId, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(adminId, task.getUserId());
    }

    @Test
    public void set() {
        repository.removeAll(userId);
        repository.removeAll(adminId);
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.set(ADMIN_TASK_LIST);
        final List<Task> tasks = emptyRepository.findAll();
        tasks.forEach(task -> Assert.assertEquals(adminId, task.getUserId()));
    }

    private int getIndexFromList(@NotNull final List<Task> tasks, @NotNull final String id) {
        int index = 0;
        for (Task task : tasks) {
            index++;
            if (id.equals(task.getId())) return index;
        }
        return -1;
    }

    @Test
    public void findAll() {
        repository.removeAll(userId);
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(USER_TASK_LIST);
        final List<Task> tasks = emptyRepository.findAll();
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Task> tasks = repository.findAll(userId);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllComparator() {
        repository.removeAll(userId);
        repository.removeAll(adminId);
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Task> tasks = emptyRepository.findAll(userId, comparator);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        final List<Task> tasks2 = emptyRepository.findAll(adminId, comparator);
        tasks2.forEach(task -> Assert.assertEquals(adminId, task.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Task> tasks = repository.findAll();
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Task> tasks = repository.findAll(userId);
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        @Nullable final Task task = repository.findOneByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void removeAll() {
        repository.removeAll(userId);
        repository.removeAll(adminId);
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(TASK_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Task task = repository.add(ADMIN_TASK1);
        Assert.assertNotNull(repository.findOneById(ADMIN_TASK1.getId()));
        repository.removeOne(task);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.add(ADMIN_TASK2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TASK2.getId()));
        repository.removeOneById(task.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(adminId, NON_EXISTING_TASK_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_TASK2.getId()));
        @Nullable final Task task = repository.add(ADMIN_TASK2);
        Assert.assertNotNull(repository.findOneById(adminId, ADMIN_TASK2.getId()));
        repository.removeOneById(task.getId());
        Assert.assertNull(repository.findOneById(adminId, ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        repository.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = repository.findAll();
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
        Assert.assertNotNull(task);
        @Nullable final Task task2 = repository.removeOneByIndex(index);
        Assert.assertNotNull(task2);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        repository.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = repository.findAll(adminId);
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        @Nullable final Task task2 = repository.removeOneByIndex(adminId, index);
        Assert.assertNotNull(task2);
        Assert.assertNull(repository.findOneById(task2.getId(), task2.getUserId()));
    }

    @Test
    public void findAllByProjectId() {
        repository.removeAll(userId);
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(USER_TASK_LIST);
        @Nullable final List<Task> tasks = emptyRepository.findAllByProjectId(USER_TASK1.getUserId(), USER_PROJECT1.getId());
        tasks.forEach(task -> Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
        @Nullable final List<Task> tasks2 = emptyRepository.findAllByProjectId(USER_TASK1.getUserId(), USER_PROJECT2.getId());
        tasks2.forEach(task -> Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId()));
    }

}

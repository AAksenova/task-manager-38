package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;
import ru.t1.aksenova.tm.util.HashUtil;

import java.sql.Connection;

import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.USER_TASK2;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository repository = new UserRepository(connection);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
//        @NotNull final User user = repository.add(USER_TEST);
//        userId = user.getId();
//        @NotNull final User admin = repository.add(ADMIN_TEST);
//        adminId = admin.getId();
////        USER_TASK1.setUserId(userId);
////        USER_TASK2.setUserId(userId);
//        ADMIN_TASK1.setUserId(adminId);
//        ADMIN_TASK2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = repository.findOneById(userId);
        if (user != null) repository.removeOne(user);
        user = repository.findOneById(adminId);
        if (user != null) repository.removeOne(user);
    }

    @Before
    public void before() {
        @NotNull final User admin = repository.add(ADMIN_TEST);
        adminId = admin.getId();
        ADMIN_TASK1.setUserId(adminId);
        ADMIN_TASK2.setUserId(adminId);
    }

    @After
    public void after() {
        @Nullable User user;
        if (repository.isLoginExist(USER_TEST_LOGIN)) {
            user = repository.findByLogin(USER_TEST_LOGIN);
            if (user != null) repository.removeOne(user);
        }
        if (repository.isLoginExist(USER_ADMIN_LOGIN)) {
            user = repository.findByLogin(USER_ADMIN_LOGIN);
            if (user != null) repository.removeOne(user);
        }
    }

    @Test
    public void create() {
        @NotNull final User user = repository.create(USER_TEST_LOGIN,  USER_TEST_PASSWORD, propertyService);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() {
        @NotNull final User user = repository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL, propertyService);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(USER_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() {
        @NotNull final User user = repository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, Role.USUAL, propertyService);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(USER_TEST.getRole(), user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @Test
    public void findByEmail() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findByEmail(ADMIN_TEST.getEmail());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @Test
    public void isLoginExist() {
        Assert.assertFalse(repository.isLoginExist(NON_EXISTING_LOGIN));
        Assert.assertTrue(repository.isLoginExist(ADMIN_TEST.getLogin()));
    }

    @Test
    public void isEmailExist() {
        Assert.assertFalse(repository.isEmailExist(NON_EXISTING_EMAIL));
        Assert.assertTrue(repository.isEmailExist(ADMIN_TEST.getEmail()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.add(USER_TEST);
        Assert.assertNotNull(repository.findOneById(USER_TEST.getId()));
        repository.removeOneById(user.getId());
        Assert.assertNull(repository.findOneById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

}

package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService, taskService, projectService);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final User user = userService.add(USER_TEST);
        userId = user.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        USER_TASK1.setUserId(userId);
        USER_TASK2.setUserId(userId);
        USER_TASK3.setUserId(userId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = userService.findOneById(userId);
        if (user != null) userService.removeOne(user);
    }

    @Before
    public void before() {
        projectService.add(USER_PROJECT1);
        projectService.add(USER_PROJECT2);
        taskService.add(USER_TASK1);
        taskService.add(USER_TASK2);
        taskService.add(USER_TASK3);
    }

    @After
    public void after() {
        taskService.removeAll(userId);
        projectService.removeAll(userId);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), ADMIN_TASK1.getId()));
        projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final Task task = taskService.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), ADMIN_TASK1.getId()));
        projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final Task task = taskService.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(null, USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, NON_EXISTING_PROJECT_ID));

        @NotNull List<Task> tasks = taskService.findAllByProjectId(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(tasks);
        projectTaskService.removeTaskToProject(userId, USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.findAllByProjectId(userId, USER_PROJECT1.getId()));
    }

}

package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.ADMIN_PROJECT1;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskService service = new TaskService(connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService, service, projectService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @BeforeClass
    public static void initData() {
        @NotNull final User user = userService.add(USER_TEST);
        userId = user.getId();
        @NotNull final User admin = userService.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_TASK1.setUserId(userId);
        USER_TASK2.setUserId(userId);
        ADMIN_TASK1.setUserId(adminId);
        ADMIN_TASK2.setUserId(adminId);
    }

    @AfterClass
    public static void clearData() {
        @Nullable User user = userService.findOneById(userId);
        if (user != null) userService.removeOne(user);
        user = userService.findOneById(adminId);
        if (user != null) userService.removeOne(user);
    }

    @Before
    public void before() {
        service.add(USER_TASK1);
        service.add(USER_TASK2);
    }

    @After
    public void after() {
        service.removeAll(userId);
        service.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> service.add(NULL_TASK));
        Assert.assertNotNull(service.add(ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task.getId(), service.findOneById(task.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, ADMIN_TASK1));
        Assert.assertThrows(AbstractException.class, () -> service.add("", ADMIN_TASK1));
        Assert.assertThrows(AbstractException.class, () -> service.add(adminId, NULL_TASK));
        Assert.assertNotNull(service.add(adminId, ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(adminId, ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(adminId, null, ADMIN_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.create(adminId, ADMIN_TASK1.getName(), null));
        @NotNull final Task task = service.create(adminId, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(adminId, task.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(AbstractException.class, () -> service.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, USER_TASK1.getId(), null, USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, USER_TASK1.getId(), USER_TASK1.getName(), null));
        @NotNull final Task task = service.updateById(userId, USER_TASK1.getId(), TASK_NAME, TASK_DESCR);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_NAME, task.getName());
        Assert.assertEquals(TASK_DESCR, task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void updateByUserIdByIndex() {
        @NotNull final List<Task> tasks = service.findAll();
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(null, index, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, null, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, -1, USER_TASK1.getName(), USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, index, null, USER_TASK1.getDescription()));
        Assert.assertThrows(AbstractException.class, () -> service.updateByIndex(userId, index, USER_TASK1.getName(), null));
        @NotNull final Task task = service.updateByIndex(userId, index, TASK_NAME, TASK_DESCR);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_NAME, task.getName());
        Assert.assertEquals(TASK_DESCR, task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(null, USER_TASK1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(userId, USER_TASK1.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(userId, NON_EXISTING_TASK_ID, Status.IN_PROGRESS));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusById(NON_EXISTING_USER_ID, USER_TASK1.getId(), Status.IN_PROGRESS));
        @NotNull final Task task = service.changeTaskStatusById(userId, USER_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final List<Task> tasks = service.findAll();
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(null, index, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(userId, null, Status.COMPLETED));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(userId, index, null));
        Assert.assertThrows(AbstractException.class, () -> service.changeTaskStatusByIndex(userId, -1, Status.COMPLETED));
        @NotNull final Task task = service.changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void set() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ITaskService emptyService = new TaskService(connectionService);
        Assert.assertEquals(EMPTY_TASK_LIST, Collections.emptyList());
        emptyService.add(USER_TASK_LIST);
        emptyService.set(ADMIN_TASK_LIST);
        final List<Task> tasks = service.findAll();
        tasks.forEach(task -> Assert.assertEquals(adminId, task.getUserId()));
    }

    private int getIndexFromList(@NotNull final List<Task> tasks, @NotNull final String id) {
        int index = 0;
        for (Task task : tasks) {
            index++;
            if (id.equals(task.getId())) return index;
        }
        return -1;
    }

    @Test
    public void findAll() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ITaskService emptyService = new TaskService(connectionService);
        emptyService.add(USER_TASK_LIST);
        final List<Task> tasks = emptyService.findAll();
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findAll(""));
        Assert.assertEquals(Collections.emptyList(), service.findAll(NON_EXISTING_USER_ID));
        final List<Task> tasks = service.findAll(userId);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllComparator() {
        service.removeAll();
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ITaskService emptyService = new TaskService(connectionService);
        emptyService.add(USER_TASK_LIST);
        emptyService.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Task> tasks = service.findAll(userId, comparator);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        final List<Task> tasks2 = service.findAll(adminId, comparator);
        tasks2.forEach(task -> Assert.assertEquals(adminId, task.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById("", USER_TASK1.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Task> tasks = service.findAll();
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(-1));
        @Nullable final Task task = service.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Task> tasks = service.findAll(userId);
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(USER_TASK1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.findOneByIndex(null, index));
        @Nullable final Task task = service.findOneByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(userId, null));
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null, USER_TASK1.getId()));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void removeAll() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ITaskService emptyService = new TaskService(connectionService);
        emptyService.removeAll();
        emptyService.add(TASK_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeOne() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOne(null));
        @Nullable final Task task = service.add(ADMIN_TASK1);
        Assert.assertNotNull(service.findOneById(ADMIN_TASK1.getId()));
        service.removeOne(task);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertNull(service.removeOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.add(ADMIN_TASK1);
        Assert.assertNotNull(service.findOneById(ADMIN_TASK1.getId()));
        service.removeOneById(task.getId());
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null, ADMIN_TASK2.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(adminId, null));
        Assert.assertNull(service.removeOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.add(ADMIN_TASK2);
        Assert.assertNotNull(service.findOneById(adminId, ADMIN_TASK2.getId()));
        service.removeOneById(task.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(adminId, ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        service.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = service.findAll();
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(-1));
        @Nullable final Task task = service.findOneByIndex(index);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
        Assert.assertNotNull(task);
        @Nullable final Task task2 = service.removeOneByIndex(index);
        Assert.assertNotNull(task2);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        @Nullable final Task task = service.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = service.findAll(adminId);
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(ADMIN_TASK1.getId(), -1));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByIndex(null, index));
        @Nullable final Task task2 = service.removeOneByIndex(adminId, index);
        Assert.assertNotNull(task2);
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(task2.getId(), task2.getUserId()));
    }

}

package ru.t1.aksenova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    private String token;

    public UserLoginResponse(@NotNull final Throwable error) {
        super(error);
    }

    public UserLoginResponse(@Nullable final String token) {
        this.token = token;
    }

}

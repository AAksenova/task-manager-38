package ru.t1.aksenova.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskRemoveByIdResponse extends AbstractResponse {

}

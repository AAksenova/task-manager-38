package ru.t1.aksenova.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
    }

    public AbstractSystemException(@Nullable final String message) {
        super(message);
    }

    public AbstractSystemException(
            @Nullable final String message,
            @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
